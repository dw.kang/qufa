[ Last Update: Oct. 6, 2022 ]

- 문화관광
files : 391
records : 4,011,462

- 재난안전
files : 2
records : 84,540

- 환경
files : 8
records : 43,715

- 보건
files : (1차)3 / (2차)4
records : (1차)7,005 / (2차)216,219

- 교통
files : 4
records : 443,437

- <sup>(new)</sup> 교육
files : 1
records : 831

- <sup>(new)</sup> 해양
files : 1
records : 111,941

- <sup>(new)</sup> 체육
files : 1
records : 82,883
