from flask import Blueprint, render_template, jsonify, request
from algorithm.qufa_algorithm import *
from urllib.request import urlopen
from urllib.parse import urlencode, quote_plus
import datetime
import pandas as pd

bp_qufa3_fa = Blueprint('qufa3_fa', __name__)

@bp_qufa3_fa.route('/api/fa/health', methods=['GET'])
def api_fa_health():
	match = 'D3E7D9D8-24E9-4698-9D1F-5098C2EE081C'
	key = request.args.get('key')
	indicator = request.args.get('indicator')

	if key != match or key is None:
		return jsonify({'result': 'error', 'message': 'invalid key'})

	if indicator is None:
		return jsonify({'result': 'error', 'message': 'invalid indicator'})

	if indicator != 'eqop' and indicator != 'eqod' and indicator != 'depa' and indicator != 'treq':
		return jsonify({'result': 'error', 'message': 'invalid indicator'})

	train, test = load_dataset(
		'http://cancerpreventionsnu.kr:3317/api/v1/heart/', '7F531E98-018F-4B82-80D4-ACE525B1AE11', 
		'http://cancerpreventionsnu.kr:3317/api/v1/heart_gan', '7F531E98-018F-4B82-80D4-ACE525B1AE11'
		)

	target = 'heart_risk10'
	subgroup = 'sex'

	fair, before_result, after_result, score = static_fair(train, test, target, subgroup, indicator)

	# fair.to_csv('output/data.csv')
	param_data = request.args.get('data')
	if param_data != None and param_data.upper() == 'FALSE':
		json_data = []
	else:
		json_data = fair.values.tolist()
	json_header = fair.columns.tolist()

	x = datetime.datetime.now()

	if indicator == 'eqop':
		json_before = {
			'comment': 'before measures',
			'tp_a': before_result.loc['tp_a', 'eqop_before'],
			'tp_b': before_result.loc['tp_b', 'eqop_before'],
			'tn_a': before_result.loc['tn_a', 'eqop_before'],
			'tn_b': before_result.loc['tn_b', 'eqop_before'],
			'fp_a': before_result.loc['fp_a', 'eqop_before'],
			'fp_b': before_result.loc['fp_b', 'eqop_before'],
			'fn_a': before_result.loc['fn_a', 'eqop_before'],
			'fn_b': before_result.loc['fn_b', 'eqop_before'],
			'tpr_a': before_result.loc['tpr_a', 'eqop_before'],
			'tpr_b': before_result.loc['tpr_b', 'eqop_before']
		}
		json_after = {
			'comment': 'after measures',
			'tp_a': after_result.loc['tp_a', 'eqop_after'],
			'tp_b': after_result.loc['tp_b', 'eqop_after'],
			'tn_a': after_result.loc['tn_a', 'eqop_after'],
			'tn_b': after_result.loc['tn_b', 'eqop_after'],
			'fp_a': after_result.loc['fp_a', 'eqop_after'],
			'fp_b': after_result.loc['fp_b', 'eqop_after'],
			'fn_a': after_result.loc['fn_a', 'eqop_after'],
			'fn_b': after_result.loc['fn_b', 'eqop_after'],
			'tpr_a': after_result.loc['tpr_a', 'eqop_after'],
			'tpr_b': after_result.loc['tpr_b', 'eqop_after']
		}
		json_ratio = {
			'comment': 'assessment of correction (tpr: equlity of opportunity)',
			'eqop': score
		}
	elif indicator == 'eqod':
		json_before = {
			'comment': 'before measures',
			'tp_a': before_result.loc['tp_a', 'eqod_before'],
			'tp_b': before_result.loc['tp_b', 'eqod_before'],
			'tn_a': before_result.loc['tn_a', 'eqod_before'],
			'tn_b': before_result.loc['tn_b', 'eqod_before'],
			'fp_a': before_result.loc['fp_a', 'eqod_before'],
			'fp_b': before_result.loc['fp_b', 'eqod_before'],
			'fn_a': before_result.loc['fn_a', 'eqod_before'],
			'fn_b': before_result.loc['fn_b', 'eqod_before'],
			'tpr_a': before_result.loc['tpr_a', 'eqod_before'],
			'fpr_a': before_result.loc['tpr_b', 'eqod_before'],
			'tpr_b': before_result.loc['fpr_a', 'eqod_before'],
			'fpr_b': before_result.loc['fpr_b', 'eqod_before']
		}
		json_after = {
			'comment': 'after measures',
			'tp_a': after_result.loc['tp_a', 'eqod_after'],
			'tp_b': after_result.loc['tp_b', 'eqod_after'],
			'tn_a': after_result.loc['tn_a', 'eqod_after'],
			'tn_b': after_result.loc['tn_b', 'eqod_after'],
			'fp_a': after_result.loc['fp_a', 'eqod_after'],
			'fp_b': after_result.loc['fp_b', 'eqod_after'],
			'fn_a': after_result.loc['fn_a', 'eqod_after'],
			'fn_b': after_result.loc['fn_b', 'eqod_after'],
			'tpr_a': after_result.loc['tpr_a', 'eqod_after'],
			'fpr_a': after_result.loc['tpr_b', 'eqod_after'],
			'tpr_b': after_result.loc['fpr_a', 'eqod_after'],
			'fpr_b': after_result.loc['fpr_b', 'eqod_after']
		}
		json_ratio = {
			'comment': 'assessment of correction (tprfpr: equlized odds)',
			'eqod': score
		}
	elif indicator == 'depa':
		json_before = {
			'comment': 'before measures',
			'tp_a': before_result.loc['tp_a', 'depa_before'],
			'tp_b': before_result.loc['tp_b', 'depa_before'],
			'tn_a': before_result.loc['tn_a', 'depa_before'],
			'tn_b': before_result.loc['tn_b', 'depa_before'],
			'fp_a': before_result.loc['fp_a', 'depa_before'],
			'fp_b': before_result.loc['fp_b', 'depa_before'],
			'fn_a': before_result.loc['fn_a', 'depa_before'],
			'fn_b': before_result.loc['fn_b', 'depa_before'],
			'dp_a': before_result.loc['de_a', 'depa_before'],
			'dp_b': before_result.loc['de_b', 'depa_before']
		}
		json_after = {
			'comment': 'after measures',
			'tp_a': after_result.loc['tp_a', 'depa_after'],
			'tp_b': after_result.loc['tp_b', 'depa_after'],
			'tn_a': after_result.loc['tn_a', 'depa_after'],
			'tn_b': after_result.loc['tn_b', 'depa_after'],
			'fp_a': after_result.loc['fp_a', 'depa_after'],
			'fp_b': after_result.loc['fp_b', 'depa_after'],
			'fn_a': after_result.loc['fn_a', 'depa_after'],
			'fn_b': after_result.loc['fn_b', 'depa_after'],
			'dp_a': after_result.loc['de_a', 'depa_after'],
			'dp_b': after_result.loc['de_b', 'depa_after']
		}
		json_ratio = {
			'comment': 'assessment of correction (dp: equalizing disincentives)',
			'depa': score
		}
	elif indicator == 'treq':
		json_before = {
			'comment': 'before measures',
			'tp_a': before_result.loc['tp_a', 'treq_before'],
			'tp_b': before_result.loc['tp_b', 'treq_before'],
			'tn_a': before_result.loc['tn_a', 'treq_before'],
			'tn_b': before_result.loc['tn_b', 'treq_before'],
			'fp_a': before_result.loc['fp_a', 'treq_before'],
			'fp_b': before_result.loc['fp_b', 'treq_before'],
			'fn_a': before_result.loc['fn_a', 'treq_before'],
			'fn_b': before_result.loc['fn_b', 'treq_before'],
			'te_a': before_result.loc['tr_a', 'treq_before'],
			'te_b': before_result.loc['tr_b', 'treq_before']
		}
		json_after = {
			'comment': 'after measures',
			'tp_a': after_result.loc['tp_a', 'treq_after'],
			'tp_b': after_result.loc['tp_b', 'treq_after'],
			'tn_a': after_result.loc['tn_a', 'treq_after'],
			'tn_b': after_result.loc['tn_b', 'treq_after'],
			'fp_a': after_result.loc['fp_a', 'treq_after'],
			'fp_b': after_result.loc['fp_b', 'treq_after'],
			'fn_a': after_result.loc['fn_a', 'treq_after'],
			'fn_b': after_result.loc['fn_b', 'treq_after'],
			'te_a': after_result.loc['tr_a', 'treq_after'],
			'te_b': after_result.loc['tr_b', 'treq_after']
		}
		json_ratio = {
			'comment': 'assessment of correction (teq: treatment equality)',
			'treq': score
		}
	else:
		json_before = {
			'comment': 'before measures',
		}
		json_after = {
			'comment': 'after measures',
		}
		json_ratio = {
			'comment': 'assessment of correction',
		}

	return jsonify({
		'result': 'success',
		'message': 'ok',
		'timestamp': x,
		'domain': 'health',
		'data': json_data,
		'header': json_header,
		'before': json_before,
		'after': json_after,
		'ratio': json_ratio
	})

