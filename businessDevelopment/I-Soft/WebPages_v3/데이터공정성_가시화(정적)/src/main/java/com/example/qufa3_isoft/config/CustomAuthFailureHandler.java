package com.example.qufa3_isoft.config;

import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

public class CustomAuthFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
                                        HttpServletResponse response,
                                        AuthenticationException exception)
            throws IOException, ServletException {
        String message;
        if (exception instanceof BadCredentialsException) {
            message = "아이디 또는 비밀번호가 맞지 않습니다. (BadCredentialsException)";
        } else if (exception instanceof UsernameNotFoundException) {
            message = "아이디가 존재하지 않습니다. (UsernameNotFoundException)";
        } else if (exception instanceof AccountExpiredException) {
            message = "계정이 만료되었습니다. (AccountExpiredException)";
        } else if (exception instanceof CredentialsExpiredException) {
            message = "비밀번호가 만료되었습니다. (CredentialsExpiredException)";
        } else if (exception instanceof DisabledException) {
            message = "계정이 비활성화 상태입니다. (DisabledException)";
        } else if (exception instanceof LockedException) {
            message = "계정이 비활성화 상태입니다. (LockedException)";
        } else if (exception instanceof InternalAuthenticationServiceException) {
            message = "인증서비스에 오류가 발생했습니다. (InternalAuthenticationServiceException)";
        } else if (exception instanceof AuthenticationCredentialsNotFoundException) {
            message = "인증이 거부되었습니다. (AuthenticationCredentialsNotFoundException)";
        } else {
            message = "오류가 발생했습니다. (기타)";
        }
        message = URLEncoder.encode(message, "UTF-8");
        setDefaultFailureUrl("/login?result=fail&message=" + message);
        super.onAuthenticationFailure(request, response, exception);
    }
}
